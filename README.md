# **Store Assignment**


# _Project Description_
Market store is a console application that allows store employees to perform cash transactions and cash receipt retrieval.

In this store you can buy different products - food, beverages, clothes, appliances.
Each product has a unit price and depending on different criteria there may be a different discount.
For example:
The perishable products (food and beverages) have expiration dates.
If those products are about to expire a discount is added - 10% if the expiration date is within 5 days of the purchase date and 50% if the product expires at the date of purchase.

For the clothes and appliances there is a 10% discount on all clothes bought during the working days, and 5% discount on all appliances that cost above $999 and are purchased during the weekend.

The final look of the recipe contains all purchased products with their price, quantity, the total sum and the total discount in the format:

### Format example

```
Date: <date and time of purchase>
---Products---


<name> <brand>
<quantity> x <price per product> = <total price without discount>
#discount <discount %> <discount sum> (if applicable)

-----------------------------------------------------------------------------------

SUBTOTAL: <total sum for all products>
DISCOUNT: - <sum of all discounts>

TOTAL: <sum to pay>
```

### Input example

```
The date and time of purchase is determined when the cashier places the order (at present time). 
The system checks the day and whether it meets the criteria specified by the store and determines the discount rate.
```

### You can try this sample input.

```
        Product beverage = new BeverageImpl(
                "Beverage", "CocaCola", 2.99, "2021-06-29");
        Product biscuits = new FoodImpl(
                "Biscuits", "Nestle", 4.99, "2021-06-27");
        Product apples = new FoodImpl(
                "Apples", "Honeycrisp", 6.99, "2021-07-12");
        Product tShirt = new ClothesImpl(
                "T-shirt", "Adidas", 20, "M", "White");
        Product jeans = new ClothesImpl(
                "Jeans", "Tom Tailor", 50, "S", "Gray");
        Product laptop = new AppliancesImpl(
                "Laptop", "HP", 1200, "Pavilion", "2021-03-03", 0.700);
        Product mouse = new AppliancesImpl(
                "Mouse", "HP", 15, "G3", "2021-02-10", 0.030);

        Cart cart = new CartImpl();
        cart.addProduct(beverage, 3);
        cart.addProduct(biscuits, 2);
        cart.addProduct(apples, 2.56);
        cart.addProduct(tShirt, 2);
        cart.addProduct(jeans, 2);
        cart.addProduct(laptop, 1);
        cart.addProduct(mouse, 1);

        Cashier cashier = new CashierImpl();
        cashier.printReceipt(cart);
```

### Output Example

```
----------Products----------

Date: 2021-06-29 17:49:30

Biscuits Nestle 
2 x $4,99 = $9,98
#Discount $10% -$1,00

Apples Honeycrisp 
2,56 x $6,99 = $17,89

Mouse HP G3 
1 x $15,00 = $15,00

Laptop HP Pavilion 
1 x $1200,00 = $1200,00

Beverage CocaCola 
3 x $2,99 = $8,97
#Discount $50% -$4,49

Jeans Tom Tailor S Gray 
2 x $50,00 = $100,00
#Discount $10% -$10,00

T-shirt Adidas M White 
2 x $20,00 = $40,00
#Discount $10% -$4,00

----------------------------
SUBTOTAL: $1391,84
DISCOUNT: -$19,48
TOTAL: $1372,36
```