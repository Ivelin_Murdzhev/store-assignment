package com.company;

import com.company.contracts.Cart;
import com.company.contracts.Cashier;
import com.company.contracts.Product;
import com.company.contracts.Writer;
import com.company.utils.ConsoleWriter;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class CashierImpl implements Cashier {

    private final LocalDateTime dateOfPurchase;
    private final Writer writer;
    private static final DecimalFormat format = new DecimalFormat("###.##");


    public CashierImpl() {
        this.dateOfPurchase = LocalDateTime.now();
        this.writer = new ConsoleWriter();
    }

    @Override
    public void printReceipt(Cart cart) {
        HashMap<Product, Double> products = cart.getProducts();
        double getDiscount = 0;

        recipeStart();
        getDiscount = getProductsInfo(products, getDiscount);
        recipeEnd(cart, getDiscount);

        writer.writeLine();
    }

    private void recipeStart() {
        writer.buildMessage("----------Products----------");
        writer.buildMessage(System.lineSeparator());
        makeNewLine();
        writer.buildMessage("Date: ");
        writer.buildMessage(dateOfPurchase.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        writer.buildMessage(System.lineSeparator());
        makeNewLine();
    }

    private double getProductsInfo(HashMap<Product, Double> products, double getDiscount) {
        for (Map.Entry<Product, Double> productEntry : products.entrySet()) {
            writer.buildMessage(productEntry.getKey().printInfo());
            writer.buildMessage(String.format("%s x $%.2f = $",
                    format.format(productEntry.getValue()), productEntry.getKey().getPrice()));
            writer.buildMessage(String.format("%.2f%n",
                    productEntry.getValue() * productEntry.getKey().getPrice()));

            if (productEntry.getKey().generateDiscount() != 0) {
                writer.buildMessage(String.format("#Discount $%.0f",
                        productEntry.getKey().generateDiscount() * 100));
                writer.buildMessage("%");
                writer.buildMessage(String.format(" -$%.2f",
                        productEntry.getKey().getDiscount() * productEntry.getValue()));
                writer.buildMessage(String.format("%n"));
            }
            makeNewLine();

            getDiscount += productEntry.getKey().getDiscount() * productEntry.getValue();
        }
        return getDiscount;
    }

    private void recipeEnd(Cart cart, double getDiscount) {
        writer.buildMessage("----------------------------");
        makeNewLine();
        writer.buildMessage(String.format("SUBTOTAL: $%.2f%n", cart.getTotalPrice()));
        writer.buildMessage(String.format("DISCOUNT: -$%.2f%n", getDiscount));
        writer.buildMessage(String.format("TOTAL: $%.2f%n", cart.getTotalPrice() - getDiscount));
    }

    private void makeNewLine() {
        writer.buildMessage(String.format("%n"));
    }
}
