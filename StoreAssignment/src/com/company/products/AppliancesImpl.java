package com.company.products;

import com.company.contracts.Appliances;

import java.time.LocalDate;
import java.util.Calendar;

import static com.company.utils.ValidationHelper.validateInputDateFormat;
import static com.company.utils.ValidationHelper.validateMinStringLength;

public class AppliancesImpl extends ProductImpl implements Appliances {

    private static final int MIN_NAME_LENGTH_VALUE = 3;
    private static final String MIN_NAME_LENGTH_ERROR_MSG =
            String.format("Appliances name must be more than %d$!", MIN_NAME_LENGTH_VALUE);

    private static final int MIN_BRAND_LENGTH_VALUE = 1;
    private static final String MIN_BRAND_LENGTH_ERROR_MSG =
            String.format("Appliances brand must be more than %d$!", MIN_BRAND_LENGTH_VALUE);

    private String model;
    private LocalDate productionDate;
    private double weight;

    public AppliancesImpl(String name, String brand, double price, String model,
                          String productionDate, double weight) {
        super(name, brand, price);
        setModel(model);
        setProductionDate(productionDate);
        setWeight(weight);
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public LocalDate getProductionDate() {
        return productionDate;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    protected void validateName(String name) {
        validateMinStringLength(name, MIN_NAME_LENGTH_VALUE, MIN_NAME_LENGTH_ERROR_MSG);
    }

    @Override
    protected void validateBrand(String brand) {
        validateMinStringLength(brand, MIN_BRAND_LENGTH_VALUE, MIN_BRAND_LENGTH_ERROR_MSG);
    }

    @Override
    protected double calculateDiscount() {
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 1 || dayOfWeek == 7 && getPrice() > 999) {
            return 0.05;
        }else
            return 0;
    }

    private void setModel(String model) {
        this.model = model;
    }

    private void setProductionDate(String productionDate) {
        this.productionDate = validateInputDateFormat(productionDate);
    }

    private void setWeight(double weight) {
        this.weight = weight;
    }
}
