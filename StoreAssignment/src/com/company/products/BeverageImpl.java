package com.company.products;

import com.company.contracts.Beverage;

public class BeverageImpl extends PerishableProductsImpl implements Beverage {

    public BeverageImpl(String name, String brand, double price, String expirationDate) {
        super(name, brand, price, expirationDate);
    }
}
