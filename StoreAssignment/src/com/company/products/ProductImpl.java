package com.company.products;

import com.company.contracts.Appliances;
import com.company.contracts.Clothes;
import com.company.contracts.Product;

import static com.company.utils.ValidationHelper.validateMinPrice;

public abstract class ProductImpl implements Product {

    private static final double MIN_PRICE_VALUE = 0.01;
    private static final String MIN_PRICE_ERROR_MSG =
            String.format("Price must be more than %.2f$!", MIN_PRICE_VALUE);

    private String name;
    private String brand;
    private double price;

    public ProductImpl(String name, String brand, double price) {
        setName(name);
        setBrand(brand);
        setPrice(price);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public double generateDiscount() {
        return calculateDiscount();
    }

    @Override
    public double getDiscount() {
        return getPrice() * generateDiscount();
    }

    protected abstract void validateName(String name);

    protected abstract void validateBrand(String brand);

    protected abstract double calculateDiscount();

    private void setName(String name) {
        validateName(name);
        this.name = name;
    }

    private void setBrand(String brand) {
        validateBrand(brand);
        this.brand = brand;
    }

    private void setPrice(double price) {
        validateMinPrice(price, MIN_PRICE_VALUE, MIN_PRICE_ERROR_MSG);
        this.price = price;
    }

    @Override
    public String printInfo() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("%s ", this.getName()));
        builder.append(String.format("%s ", this.getBrand()));

        checkInstanceClothes(builder);
        checkInstanceAppliances(builder);

        builder.append(String.format("%n"));
        return builder.toString();
    }

    private void checkInstanceClothes(StringBuilder builder) {
        if (this instanceof Clothes) {
            builder.append(String.format("%s ", ((Clothes) this).getClothesSize()));
            builder.append(String.format("%s ", ((Clothes) this).getColor()));
        }
    }

    private void checkInstanceAppliances(StringBuilder builder) {
        if (this instanceof Appliances) {
            builder.append(String.format("%s ", ((Appliances) this).getModel()));
        }
    }
}
