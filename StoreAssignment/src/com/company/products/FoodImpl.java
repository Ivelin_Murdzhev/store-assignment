package com.company.products;

import com.company.contracts.Food;

public class FoodImpl extends PerishableProductsImpl implements Food {

    public FoodImpl(String name, String brand, double price, String expirationDate) {
        super(name, brand, price, expirationDate);
    }
}
