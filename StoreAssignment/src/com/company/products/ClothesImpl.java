package com.company.products;

import com.company.common.ClothesSize;
import com.company.contracts.Clothes;

import java.util.Calendar;

import static com.company.utils.ValidationHelper.validateMinStringLength;

public class ClothesImpl extends ProductImpl implements Clothes {

    private static final int MIN_NAME_LENGTH_VALUE = 2;
    private static final String MIN_NAME_LENGTH_ERROR_MSG =
            String.format("Clothes name must be more than %d$!", MIN_NAME_LENGTH_VALUE);

    private static final int MIN_BRAND_LENGTH_VALUE = 1;
    private static final String MIN_BRAND_LENGTH_ERROR_MSG =
            String.format("Clothes brand must be more than %d$!", MIN_BRAND_LENGTH_VALUE);
    private static final double TEN_PERCENT = 0.1;

    private String color;
    private ClothesSize size;

    public ClothesImpl(String name, String brand, double price, String size, String color) {
        super(name, brand, price);
        setSize(size);
        setColor(color);
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public ClothesSize getClothesSize() {
        return size;
    }

    @Override
    protected void validateName(String name) {
        validateMinStringLength(name, MIN_NAME_LENGTH_VALUE, MIN_NAME_LENGTH_ERROR_MSG);
    }

    @Override
    protected void validateBrand(String brand) {
        validateMinStringLength(brand, MIN_BRAND_LENGTH_VALUE, MIN_BRAND_LENGTH_ERROR_MSG);
    }

    @Override
    protected double calculateDiscount() {
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 1 || dayOfWeek == 7) {
            return 0;
        } else
            return TEN_PERCENT;
    }

    private void setColor(String color) {
        this.color = color;
    }

    private void setSize(String size) {
        this.size = ClothesSize.valueOf(size);
    }
}
