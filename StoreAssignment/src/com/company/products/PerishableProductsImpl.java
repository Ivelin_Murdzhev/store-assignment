package com.company.products;

import com.company.contracts.PerishableProducts;

import java.time.LocalDate;

import static com.company.utils.ValidationHelper.validateInputDateFormat;
import static com.company.utils.ValidationHelper.validateMinStringLength;
import static java.time.temporal.ChronoUnit.DAYS;

public abstract class PerishableProductsImpl extends ProductImpl implements PerishableProducts {

    private static final int MIN_NAME_LENGTH_VALUE = 2;
    private static final String MIN_NAME_LENGTH_ERROR_MSG =
            String.format("The Food/Beverage name must be more than %d$!", MIN_NAME_LENGTH_VALUE);

    private static final int MIN_BRAND_LENGTH_VALUE = 1;
    private static final String MIN_BRAND_LENGTH_ERROR_MSG =
            String.format("The Food/Beverage brand must be more than %d$!", MIN_BRAND_LENGTH_VALUE);
    private static final double TEN_PERCENT = 0.1;
    private static final double FIFTY_PERCENT = 0.5;

    private LocalDate expirationDate;

    public PerishableProductsImpl(String name, String brand, double price, String expirationDate) {
        super(name, brand, price);
        setExpirationDate(expirationDate);
    }

    @Override
    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    @Override
    protected void validateName(String name) {
        validateMinStringLength(name, MIN_NAME_LENGTH_VALUE, MIN_NAME_LENGTH_ERROR_MSG);
    }

    @Override
    protected void validateBrand(String brand) {
        validateMinStringLength(brand, MIN_BRAND_LENGTH_VALUE, MIN_BRAND_LENGTH_ERROR_MSG);
    }

    @Override
    protected double calculateDiscount() {
        if (checkDays(LocalDate.now(), getExpirationDate()) == 1) {
            return TEN_PERCENT;
        } else if (getExpirationDate().isEqual(LocalDate.now())) {
            return FIFTY_PERCENT;
        } else {
            return 0;
        }
    }

    private void setExpirationDate(String expirationDate) {
        this.expirationDate = validateInputDateFormat(expirationDate);
    }

    private int checkDays(LocalDate purchase, LocalDate expirationDate) {
        long difference = DAYS.between(purchase, expirationDate);
        if (difference > -6 && difference < 0) {
            return 1;
        } else return 0;
    }
}
