package com.company.contracts;

import java.time.LocalDate;

public interface PerishableProducts extends Product {

    LocalDate getExpirationDate();
}
