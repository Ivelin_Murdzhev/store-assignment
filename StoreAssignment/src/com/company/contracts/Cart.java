package com.company.contracts;

import java.util.HashMap;

public interface Cart {

    void addProduct(Product product, double quantity);

    HashMap<Product, Double> getProducts();

    double getTotalPrice();
}
