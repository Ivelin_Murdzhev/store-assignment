package com.company.contracts;

import com.company.common.ClothesSize;

public interface Clothes extends Product {

    String getColor();

    ClothesSize getClothesSize();
}
