package com.company.contracts;

public interface Product {

    String getName();

    String getBrand();

    double getPrice();

    double generateDiscount();

    double getDiscount();

    String printInfo();
}
