package com.company.contracts;

public interface Cashier {

    void printReceipt(Cart cart);
}
