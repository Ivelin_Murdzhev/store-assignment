package com.company.contracts;

public interface Writer {

    void buildMessage(String message);

    void writeLine();
}
