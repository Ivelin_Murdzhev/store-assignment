package com.company.contracts;

import java.time.LocalDate;

public interface Appliances extends Product {

    String getModel();

    LocalDate getProductionDate();

    double getWeight();
}
