package com.company.cart;

import com.company.contracts.Cart;
import com.company.contracts.Product;

import java.util.HashMap;
import java.util.Map;

import static com.company.utils.ValidationHelper.validateNotNull;

public class CartImpl implements Cart {

    private static final String NULL_ERROR_MSN = "Product cannot be null!";
    public static final String QUANTITY_ERROR_MSN = "Choose quantity that start from 1 to above!";

    private final HashMap<Product, Double> products;

    public CartImpl() {
        this.products = new HashMap<>();
    }

    @Override
    public void addProduct(Product product, double quantity) {
        validateNotNull(product, NULL_ERROR_MSN);
        validateQuantity(quantity);
        products.put(product, quantity);
    }

    @Override
    public HashMap<Product, Double> getProducts() {
        return new HashMap<>(products);
    }

    @Override
    public double getTotalPrice() {
        double totalPrice = 0;
        for (Map.Entry<Product, Double> productDoubleEntry : products.entrySet()) {
            totalPrice += productDoubleEntry.getKey().getPrice() * productDoubleEntry.getValue();
        }
        return totalPrice;
    }

    private void validateQuantity(double quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException(QUANTITY_ERROR_MSN);
        }
    }
}
