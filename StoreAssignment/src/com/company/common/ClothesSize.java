package com.company.common;

public enum ClothesSize {
    XS,
    S,
    M,
    L,
    XL;

    @Override
    public String toString() {
        switch (this) {
            case XS:
                return "XS";
            case S:
                return "S";
            case M:
                return "M";
            case L:
                return "L";
            case XL:
                return "XL";
            default:
                throw new IllegalArgumentException("Incorrect size detected!");
        }
    }
}
