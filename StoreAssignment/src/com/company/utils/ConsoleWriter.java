package com.company.utils;

import com.company.contracts.Writer;

public class ConsoleWriter implements Writer {

    private final StringBuilder builder;

    public ConsoleWriter() {
        builder = new StringBuilder();
    }

    @Override
    public void buildMessage(String message) {
        builder.append(message);
    }

    @Override
    public void writeLine() {
        System.out.println(builder);
    }
}
