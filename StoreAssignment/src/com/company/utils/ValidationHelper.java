package com.company.utils;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class ValidationHelper {

    private static final String NULL_ERROR_MSG = "String cannot be null!";
    private static final String EMPTY_OR_BLANK_ERROR_MSG = "String cannot be empty of blank!";
    private static final String DATE_ERROR_MSN =
            "Incorrect date format detected! It must be in format: *YEAR-MONTH-DAY*";

    private ValidationHelper() {

    }

    public static void validateMinPrice(double price, double minPrice, String error) {
        if (price < minPrice) {
            throw new IllegalArgumentException(error);
        }
    }

    public static void validateMinStringLength(String value, int minLength, String error) {
        validateStringNotEmpty(value);
        if (value.length() < minLength) {
            throw new IllegalArgumentException(error);
        }
    }

    public static LocalDate validateInputDateFormat(String input) {
        validateNotNull(input, NULL_ERROR_MSG);
        LocalDate date;
        try {
            date = LocalDate.parse(input);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException(DATE_ERROR_MSN);
        }
        return date;
    }

    public static void validateNotNull(Object object, String error) {
        if (object == null) {
            throw new IllegalArgumentException(error);
        }
    }

    private static void validateStringNotEmpty(String value) {
        validateNotNull(value, NULL_ERROR_MSG);
        if (value.isEmpty() || value.isBlank()) {
            throw new IllegalArgumentException(EMPTY_OR_BLANK_ERROR_MSG);
        }
    }
}
